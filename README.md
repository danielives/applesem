# Repo for my Apple in the Enterprise Seminar stuff

## Requirements for demo machine

1. Install Remote Desktop Connection
2. Install AWSCLI - see http://docs.aws.amazon.com/cli/latest/userguide/installing.html
3. Ensure Terminal's default font size is nice and big
4. Configure AWSCLI (with secret credentials)
5. Ensure my Boxstarter.pem file is available
