<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Simple PHP App</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <style>body {margin-top: 40px; background-color: #333;}</style>
        <link href="assets/css/bootstrap-responsive.min.css" rel="stylesheet">
        <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    </head>

    <body>
        <div class="container">
            <div class="hero-unit">
                <h1>Simple PHP App</h1>
                <h2>Welcome</h2>
                <p>This PHP application is running on an EC2 (Elastic Compute Cloud) instance on Amazon Web Services.</p>
                <p>The server is running PHP version <?php echo phpversion(); ?>.</p>
                <?php
	                require('user_info.php');
    	            $c_info = new Users_info;
            	    $os = $c_info->c_OS();
            	    if (substr($os, 0, 7) === 'Windows') {
            	    	echo 'Now what are you doing browsing to it from a Windows box? This is an Apple Seminar!';
            	    }
            	    else {
            	    	echo 'Enjoy!';
            	    }
                ?>
            </div>
        </div>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>

</html>